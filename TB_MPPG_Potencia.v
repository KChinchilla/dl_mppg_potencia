`timescale 1ns / 1ps

module TB_MPPG_Potencia;

	// Inputs
	reg clk;
	reg reset;
	reg [3:0] num;

	// Outputs
	wire [31:0] resultado;

	// Instantiate the Unit Under Test (UUT)
	Main uut (
		.clk(clk), 
		.reset(reset), 
		.num(num), 
		.resultado(resultado)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		num = 3'b100;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
	end
	always
		begin
			#100;
			clk= !clk;
		end
      
endmodule

