`timescale 1ns / 1ps

module ALU(
		input [2:0]operacion,
		input [31:0]read_data1,
		input [31:0]read_data2,
		output is_equal,
		output reg [31:0]resultado
	 );

always @(operacion or read_data1, read_data2)
	begin
		case (operacion)
			3'b000: resultado = read_data1 + read_data2;
			3'b001: resultado = read_data1 - read_data2;
			3'b010: resultado = read_data1 * read_data2;
			3'b011: resultado = read_data1 / read_data2;
			3'b100: resultado = read_data1 & read_data2;
			default:
				resultado = 32'hxxxx;
		endcase
end

assign is_equal = (read_data1==read_data2);

endmodule
