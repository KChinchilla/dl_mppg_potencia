`timescale 1ns / 1ps

module Potencia(
	input clk,
	input [3:0]num,
	input reset,
	input is_equal,
	output reg write_enable,
	output reg [1:0]read_addr1,
	output reg [1:0]read_addr2,
	output reg [1:0]write_addr,
	output reg w_data_selection,
	output reg w_imm,
	output reg [2:0]operacion,
	output reg r_imm
	 );

reg [2:0] est_act;
reg [2:0] est_sig;

always @(est_act)
begin
	case (est_act)
		3'b000: // Inicializar Valor
			begin
				write_enable = 1'b1;
				read_addr1=2'bxx;
				read_addr2=2'bxx;
				write_addr=2'b00;
				w_data_selection=1'b1;
				w_imm=num;
				operacion=3'bxxx;
				r_imm=1'bx;
				est_sig = 3'b001;
			end
		3'b001: // Inicializar Numero
			begin
				write_enable = 1'b1;
				read_addr1=2'bxx;
				read_addr2=2'bxx;
				write_addr=2'b01;
				w_data_selection=1'b1;
				w_imm=1'b1;
				operacion=3'bxxx;
				r_imm=1'bx;
				est_sig = 3'b010;
			end
		3'b010: // Multiplicacion
			begin
				write_enable = 1'b1;
				read_addr1=2'b00;
				read_addr2=2'b01;
				write_addr=2'b01;
				w_data_selection=1'b0;
				w_imm=1'bx;
				operacion=3'b010;
				r_imm=1'bx;
				est_sig = 3'b011;
			end
		3'b011: // Restar 1 a Numero Inicial
			begin
				write_enable = 1'b1;
				read_addr1=2'b00;
				read_addr2=2'bxx;
				write_addr=2'b00;
				w_data_selection=1'b0;
				w_imm=1'b1;
				operacion=3'b001;
				r_imm=1'b1;
				est_sig = 3'b100;
			end
		3'b100: // Comparacion si es distinto a 1
			begin
				write_enable = 1'b0;
				read_addr1=2'b00;
				read_addr2=2'bxx;
				write_addr=2'bxx;
				w_data_selection=1'bx;
				w_imm=1'bx;
				operacion=1'bx;
				r_imm=1'b1;
				est_sig = 3'b101;
			end
		3'b101: // Comparacion
			begin
				if (is_equal)
					est_sig = 3'b110;
				else
					est_sig = 3'b010;
			end
		3'b110: // Final
			begin
				write_enable = 1'bx;
				read_addr1=2'b01;
				read_addr2=2'bxx;
				write_addr=2'bxx;
				w_data_selection=1'bx;
				w_imm=1'bx;
				operacion=1'bx;
				r_imm=1'bx;
				est_sig = 3'b110;
			end
	endcase
end

always @(posedge clk)
	begin
		if (reset)
			est_act = 4'b0000;
		else
			est_act = est_sig;
	end

endmodule
