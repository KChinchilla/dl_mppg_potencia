`timescale 1ns / 1ps

module Maq_Pro(
		input clk,
		input write_enable,
		input [1:0]read_addr1,
		input [1:0]read_addr2,
		input [1:0]write_addr,
		input w_data_selection,
		input w_imm,
		input [2:0]operacion,
		input r_imm,
		output [31:0]maq_pro_salida,
		output is_equal
	 );

wire [31:0] write_data;
wire [31:0] read_data1;
wire [31:0] read_data2;
wire [31:0] resultado;
wire [31:0] read_data_2_mux;

assign resultado = read_data1;
assign read_data_2_mux = r_imm ? w_imm:read_data_2_mux;
assign write_data = w_data_selection ? w_imm:resultado;

Reg_File regfile (clk, read_addr1, read_addr2, write_addr, write_data, write_enable, read_data1, read_data2);
ALU alu (operacion, read_data1, read_data2, is_equal, resultado);

endmodule
