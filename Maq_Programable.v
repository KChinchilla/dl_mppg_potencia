`timescale 1ns / 1ps

module Reg_File(
	input clk,
	input [1:0]read_addr1,
	input [1:0]read_addr2,
	input [1:0]write_addr,
	input [31:0]write_data,
	input write_enable,
	output [31:0]read_data1,
	output [31:0]read_data2
	 );

reg [3:0]regfile[31:0];

assign read_data1 = regfile[read_addr1];
assign read_data2 = regfile[read_addr1];

always @(posedge clk)
begin
	if (write_enable)
		regfile[write_addr] = write_data;
end

endmodule
