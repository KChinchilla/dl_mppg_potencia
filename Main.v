`timescale 1ns / 1ps

module Main(
    input clk,
	 input reset,
	 input [3:0]num,
	 output [31:0] resultado
	 );
	
	wire write_enable;
	wire is_equal;
	wire [1:0]read_addr1;
	wire [1:0]read_addr2;
	wire [1:0]write_addr;
	wire w_data_selection;
	wire w_imm;
	wire [2:0]operacion;
	wire r_imm;
	wire [31:0]maq_pro_salida;
	wire clockDiv;
	
	ClkDiv clock (clk, clockDiv);
	Potencia poten(clk, num, reset, is_equal, write_enable, read_addr1, read_addr2, write_addr, w_data_selection, w_imm, operacion, r_imm);
	Maq_Pro mppg(clk, write_enable, read_addr1, read_addr2, write_addr, w_data_selection, w_imm, operacion, r_imm, maq_pro_salida, is_equal);
		
endmodule
